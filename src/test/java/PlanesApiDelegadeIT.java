
import cz.fel.storedav.swa.api.model.PlaneDto;
import cz.fel.storedav.swa.planes.Application;
import cz.fel.storedav.swa.planes.model.Plane;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.swagger.v3.oas.models.SpecVersion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import static org.junit.Assert.*;
import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(locations = "classpath:test.properties")
public class PlanesApiDelegadeIT {

    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @BeforeEach
    public void testSetup(){
        RestAssured.port = port;
    }

    @Test
    @Sql(scripts = "/test-data-empty.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_getPlanes_noData() {
        get(uri+"/planes").then().assertThat().statusCode(204);
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_getPlanes_listOfShorts(){
        RestAssured.given().when()
                .get("/planes")
                .then()
                .statusCode(200)
                .body("[0].type", equalTo("Cessna 172"))
                .body("[0].registration", equalTo("OK-EYE"))
                .body("[0].active", equalTo(true))
                .body("[0].plane", equalTo(uri+"/plane/1"))
                .body("[3].type", equalTo("Bristell 5"))
                .body("[3].registration", equalTo("OK-YAI25"))
                .body("[3].active", equalTo(false))
                .body("[3].plane", equalTo(uri+"/plane/4"));
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_putPlane_getPlanes_listOfShorts(){
        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"type\": \"Samba xxl\",\n" +
                        "  \"registration\": \"ok-mus15\",\n" +
                        "  \"category\": \"ull\",\n" +
                        "  \"seatsNumber\": 2,\n" +
                        "  \"desc\": \"old ull\",\n" +
                        "  \"active\": true\n" +
                        "}")
                .when()
                .post("/planes")
                .then()
                .statusCode(201);
//                .body(equalTo(uri+"/plane/5"));

        RestAssured.given().when()
                .get("/planes")
                .then()
                .statusCode(200)
                .body("[4].type", equalTo("Samba xxl"))
                .body("[4].registration", equalTo("ok-mus15"))
                .body("[4].active", equalTo(true))
                .body("[4].plane", equalTo(uri+"/plane/5"));
    }

    @Test
    @Sql(scripts = "/test-data-1.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void test_putPlane_duplicate(){
        RestAssured.given()
                .header("Content-type", "application/json")
                .and()
                .body("{\n" +
                        "  \"type\": \"Samba xxl\",\n" +
                        "  \"registration\": \"OK-WAR18\",\n" +
                        "  \"category\": \"ull\",\n" +
                        "  \"seatsNumber\": 2,\n" +
                        "  \"desc\": \"new ull\",\n" +
                        "  \"active\": true\n" +
                        "}")
                .when()
                .post("/planes")
                .then()
                .statusCode(409);
    }

}
