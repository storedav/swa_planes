DELETE FROM planes;
DELETE FROM plane_prices;
ALTER TABLE planes ALTER COLUMN id RESTART WITH 1;
ALTER TABLE plane_prices ALTER COLUMN id RESTART WITH 1;

INSERT INTO planes (category, desc, registration, seats_nr, type, active) VALUES ('ppl', 'Cessna.. What else to say', 'OK-EYE', 4, 'Cessna 172', true);
INSERT INTO planes (category, desc, registration, seats_nr, type, active) VALUES ('ppl', 'Smaller cessna.', 'OK-TEK', 2, 'Cessna 152', true);
INSERT INTO planes (category, desc, registration, seats_nr, type, active) VALUES ('ull', 'Good ultralight', 'OK-WAR18', 2, 'Bristell 5', true);
INSERT INTO planes (category, desc, registration, seats_nr, type, active) VALUES ('ull', '', 'OK-YAI25', 2, 'Bristell 5', false);

