package cz.fel.storedav.swa.planes.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface PlanePriceRepository extends JpaRepository<PlanePrice, Integer> {

    Optional<PlanePrice> findFirstByPlaneIdIsAndStartDateLessThanEqualOrderByStartDateDesc(Integer planeId, LocalDate queryDate);
    List<PlanePrice> findAllByPlaneIdIsAndStartDateGreaterThanEqualAndStartDateLessThanEqualOrderByStartDateAsc(Integer planeId, LocalDate startDate, LocalDate endDate);

    Optional<PlanePrice> findFirstByPlaneIdIsAndStartDateGreaterThanEqualOrderByStartDateAsc(Integer planeId, LocalDate startDate);
}
