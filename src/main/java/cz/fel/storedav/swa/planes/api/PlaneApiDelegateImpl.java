package cz.fel.storedav.swa.planes.api;

import cz.fel.storedav.swa.api.PlaneApiDelegate;
import cz.fel.storedav.swa.api.model.PlaneDto;
import cz.fel.storedav.swa.api.model.PlanePriceDto;
import cz.fel.storedav.swa.planes.model.Plane;
import cz.fel.storedav.swa.planes.model.PlanePrice;
import cz.fel.storedav.swa.planes.model.PlanePriceRepository;
import cz.fel.storedav.swa.planes.model.PlaneRepository;
import cz.fel.storedav.swa.planes.model.mappers.PlaneMapper;
import cz.fel.storedav.swa.planes.model.mappers.PlanePriceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class PlaneApiDelegateImpl implements PlaneApiDelegate {

    final Logger logger = LoggerFactory.getLogger(PlaneApiDelegateImpl.class);
    private final PlaneRepository planeRepo;

    private final PlanePriceRepository pricesRepo;

    public PlaneApiDelegateImpl(PlaneRepository planeRepo, PlanePriceRepository pricesRepo) {
        this.planeRepo = planeRepo;
        this.pricesRepo = pricesRepo;
    }

    @Override
    public ResponseEntity<PlaneDto> planePlaneIdGet(Integer planeId) {
        logger.trace("GET plane request, id: " + planeId);
        Optional<Plane> plane = planeRepo.findById(planeId);

        if (!plane.isPresent()) {
            logger.info("Plane " + planeId + " not found.");
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Plane not found."
            );
        }
        return ResponseEntity.ok(PlaneMapper.INSTANCE.mapTo(plane.get()));
    }

    @Override
    public ResponseEntity<Void> planePlaneIdPut(Integer planeId, PlaneDto planeDto) {
        logger.info("PUT plane request, id: " + planeId);

        Optional<Plane> currentPlane = planeRepo.findById(planeId);
        if (!currentPlane.isPresent()) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Plane with id " + planeId +" not found."
            );
        }

        Plane newPlane = PlaneMapper.INSTANCE.mapTo(planeDto, currentPlane.get().getId());

        if (!newPlane.getRegistration().equals(currentPlane.get().getRegistration())
                && planeRepo.countByRegistration(newPlane.getRegistration()) > 0) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Plane with same registration already exists."
            );
        }

        planeRepo.save(newPlane);

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<PlanePriceDto>> planePlaneIdPricesGet(Integer planeId, LocalDate dateStart, LocalDate dateEnd) {
        logger.trace("GET plane prices request, plane id: " + planeId);

        LocalDate qStartDate = LocalDate.of(1970,1,1);
        LocalDate qEndDate = LocalDate.of(2100, 12, 31);

        if(dateStart != null){
            Optional<PlanePrice> firstPrice = pricesRepo.findFirstByPlaneIdIsAndStartDateLessThanEqualOrderByStartDateDesc(planeId, dateStart);
            if(firstPrice.isPresent()){
                qStartDate = firstPrice.get().getStartDate();
            }
        }

        if(dateEnd != null){
            qEndDate = dateEnd;
        }

        logger.trace("before prices list, start_date = " + qStartDate.format(DateTimeFormatter.ISO_DATE) + ", end_date = " + qEndDate.format(DateTimeFormatter.ISO_DATE));

        List<PlanePriceDto> pricesList = pricesRepo.findAllByPlaneIdIsAndStartDateGreaterThanEqualAndStartDateLessThanEqualOrderByStartDateAsc(planeId, qStartDate, qEndDate)
                .stream()
                .map(price -> PlanePriceMapper.INSTANCE.mapTo(price))
                .toList();

        logger.trace("after prices list, size = " + pricesList.size());

        return ResponseEntity.ok(pricesList);
    }

    @Override
    public ResponseEntity<URI> planePlaneIdPricesPost(Integer planeId, PlanePriceDto planePriceDto) {
        logger.trace("POST plane price request, plane id: " + planeId);

        if(!planeRepo.findById(planeId).isPresent()){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Plane with specified id does not exists."
            );
        }

        Optional<PlanePrice> conflictingPrice = pricesRepo.findFirstByPlaneIdIsAndStartDateGreaterThanEqualOrderByStartDateAsc(planeId, planePriceDto.getStartDate());
        if(conflictingPrice.isPresent()){
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Newer price for the specified plane already exists."
            );
        }

        PlanePrice savedPrice = pricesRepo.save(PlanePriceMapper.INSTANCE.mapTo(planePriceDto, planeId));

        final URI priceUri =
                ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/plane/")
                        .path(planeId.toString())
                        .path("/price/")
                        .path(savedPrice.getId().toString())
                        .build().toUri();

        return ResponseEntity.created(priceUri).body(priceUri);
    }

    @Override
    public ResponseEntity<PlanePriceDto> planePlaneIdPricePriceIdGet(Integer planeId, Integer priceId) {

        Optional<PlanePrice> price = pricesRepo.findById(priceId);

        if(!price.isPresent() || !price.get().getPlaneId().equals(planeId)){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Plane or its price was not found."
            );
        }

        return ResponseEntity.ok(PlanePriceMapper.INSTANCE.mapTo(price.get()));
    }
}
