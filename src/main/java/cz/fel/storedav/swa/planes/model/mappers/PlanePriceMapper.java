package cz.fel.storedav.swa.planes.model.mappers;

import cz.fel.storedav.swa.api.model.PlanePriceDto;
import cz.fel.storedav.swa.planes.model.PlanePrice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PlanePriceMapper {

    public static PlanePriceMapper INSTANCE = Mappers.getMapper(PlanePriceMapper.class);

    @Mapping(target="planeId", source = "planeId")
    @Mapping(target="price", source = "ppDto.price")
    @Mapping(target="startDate", source = "ppDto.startDate")
    PlanePrice mapTo(PlanePriceDto ppDto, Integer planeId);
    @Mapping(target="price", source = "price.price")
    @Mapping(target="startDate", source = "price.startDate")
    PlanePriceDto mapTo(PlanePrice price);
}
