package cz.fel.storedav.swa.planes.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "planes")
public class Plane {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "type")
    @NotEmpty
    private String type;

    @Column(name = "registration", unique=true)
    @NotEmpty
    private String registration;

    @Column(name = "category")
    @NotEmpty
    private String category;

    @Column(name = "seats_nr")
    @Min(1)
    private Integer seatsNr;

    @Column(name = "plane_desc")
    private String description;

    @Column(name = "active")
    private Boolean active;

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getRegistration() {
        return registration;
    }

    public String getCategory() {
        return category;
    }

    public Integer getSeatsNr() {
        return seatsNr;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSeatsNr(Integer seatsNr) {
        this.seatsNr = seatsNr;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
