
package cz.fel.storedav.swa.planes.model.mappers;

import cz.fel.storedav.swa.api.model.PlaneDto;
import cz.fel.storedav.swa.api.model.PlaneShortDto;
import cz.fel.storedav.swa.planes.model.Plane;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.web.util.UriTemplate;

@Mapper
public interface PlaneMapper {
    public static PlaneMapper INSTANCE = Mappers.getMapper(PlaneMapper.class);

    @Mapping(target="id", source = "id")
    @Mapping(target="type", source = "planeDto.type")
    @Mapping(target="registration", source = "planeDto.registration")
    @Mapping(target="category", source = "planeDto.category")
    @Mapping(target="seatsNr", source = "planeDto.seatsNumber")
    @Mapping(target="description", source = "planeDto.desc")
    @Mapping(target="active", source = "planeDto.active")
    Plane mapTo(PlaneDto planeDto, Integer id);
    @Mapping(target="type", source = "plane.type")
    @Mapping(target="registration", source = "plane.registration")
    @Mapping(target="category", source = "plane.category")
    @Mapping(target="seatsNumber", source = "plane.seatsNr")
    @Mapping(target="desc", source = "plane.description")
    @Mapping(target="active", source = "plane.active")
    PlaneDto mapTo(Plane plane);


    @Mapping(target="type", source = "plane.type")
    @Mapping(target="registration", source = "plane.registration")
    @Mapping(target="plane", expression = "java(base_uri.expand(plane.getId()))")
    @Mapping(target="active", source = "plane.active")
    PlaneShortDto mapToShort(Plane plane, UriTemplate base_uri);


}
