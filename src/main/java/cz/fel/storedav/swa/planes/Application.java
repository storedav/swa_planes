package cz.fel.storedav.swa.planes;

import com.fasterxml.jackson.databind.Module;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(
        nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@ComponentScan(
        basePackages = {"cz.fel.storedav.swa", "cz.fel.storedav.swa.api" , "org.openapitools.configuration"},
        nameGenerator = FullyQualifiedAnnotationBeanNameGenerator.class
)
@EnableDiscoveryClient
public class Application {

    @Autowired
    private DiscoveryClient discoveryClient;
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "cz.fel.storedav.swa.Application.jsonNullableModule")
    public Module jsonNullableModule() {
        return new JsonNullableModule();
    }
}
