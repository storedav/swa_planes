package cz.fel.storedav.swa.planes.api;

import cz.fel.storedav.swa.api.PlanesApiDelegate;
import cz.fel.storedav.swa.api.model.PlaneDto;
import cz.fel.storedav.swa.api.model.PlaneShortDto;
import cz.fel.storedav.swa.planes.model.Plane;
import cz.fel.storedav.swa.planes.model.PlaneRepository;
import cz.fel.storedav.swa.planes.model.mappers.PlaneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.util.List;

@Service
public class PlanesApiDelegateImpl implements PlanesApiDelegate {
    final Logger logger = LoggerFactory.getLogger(PlanesApiDelegateImpl.class);

    private final PlaneRepository planeRepo;

    public PlanesApiDelegateImpl(PlaneRepository planeRepo) {
        this.planeRepo = planeRepo;
    }

    @Override
    public ResponseEntity<List<PlaneShortDto>> planesGet(String type, String registration, String category) {
        logger.trace("GET planes list");

        logger.trace("type: " + type + ", reg: " + registration + ", cat: " + category);

        List<Plane> foundPlanes = planeRepo.findByTypeLikeAndRegistrationLikeAndCategoryLike(
                type == null || type.isEmpty() ? "%" : type,
                registration == null || registration.isEmpty() ? "%" : registration,
                category == null || category.isEmpty() ? "%" : category);

        if (foundPlanes.isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "No plane with specified parameters was found."
            );
        }

        final UriTemplate planeUriTeamplate = new UriTemplate(ServletUriComponentsBuilder.fromCurrentContextPath().path("/plane/").toUriString() + "{planeId}");

        final List<PlaneShortDto> planes = foundPlanes.stream().map(
                plane -> PlaneMapper.INSTANCE.mapToShort(plane, planeUriTeamplate)
        ).toList();

        return ResponseEntity.ok(planes);
    }

    @Override
    public ResponseEntity<URI> planesPost(PlaneDto planeDto) {
        logger.trace("POST new plane into planes list");

        logger.trace("Plane reg: " + planeDto.getRegistration() + ", type: " + planeDto.getType());

        if (planeRepo.countByRegistration(planeDto.getRegistration()) > 0) {
            logger.info("Plane with registration " + planeDto.getRegistration() + " already exists.");
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Other plane with the same registration already exists."
            );
        }

        Plane plane = PlaneMapper.INSTANCE.mapTo(planeDto, null);

        Plane savedPlane = planeRepo.save(plane);

        final URI planeUri =
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/plane/").path(savedPlane.getId().toString()).build().toUri();
        return ResponseEntity.created(planeUri).body(planeUri);
    }
}
