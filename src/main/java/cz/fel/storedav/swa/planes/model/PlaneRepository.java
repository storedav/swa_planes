package cz.fel.storedav.swa.planes.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaneRepository extends JpaRepository<Plane, Integer>{
    long countByRegistration(String registration);

    List<Plane> findByTypeLikeAndRegistrationLikeAndCategoryLike(String type, String registration, String category);
}
