package cz.fel.storedav.swa.planes.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Entity
@Table(name = "plane_prices")
public class PlanePrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "plane_id")
    private Integer planeId;

    @Column(name = "price")
    @Min(0)
    private Integer price;

    @Column(name = "start_date")
    private LocalDate startDate;

    public Integer getId() {
        return id;
    }

    public Integer getPlaneId() {
        return planeId;
    }

    public Integer getPrice() {
        return price;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPlaneId(Integer planeId) {
        this.planeId = planeId;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
}
