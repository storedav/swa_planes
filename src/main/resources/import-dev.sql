INSERT INTO planes (category, plane_desc, registration, seats_nr, type, active) VALUES ('ppl', 'Cessna.. What else to say', 'OK-EYE', 4, 'Cessna 172', true);
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (1, 10, '2024-01-01');
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (1, 14, '2024-01-02');

INSERT INTO planes (category, plane_desc, registration, seats_nr, type, active) VALUES ('ppl', 'Smaller cessna.', 'OK-TEK', 2, 'Cessna 152', true);
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (2, 11, '2024-01-01');
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (2, 15, '2025-01-01');


INSERT INTO planes (category, plane_desc, registration, seats_nr, type, active) VALUES ('ull', 'Good ultralight', 'OK-WAR18', 2, 'Bristell 5', true);
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (3, 12, '2024-01-01');
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (3, 15, '2024-01-01');
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (3, 16, '2024-01-01');

INSERT INTO planes (category, plane_desc, registration, seats_nr, type, active) VALUES ('ull', '', 'OK-YAI25', 2, 'Bristell 5', true);
INSERT INTO plane_prices (plane_id, price, start_date) VALUES (4, 13, '2024-01-01');
